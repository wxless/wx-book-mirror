### 介绍

本小程序是基于 `微信云开发` 构建的图书资源检索下载小程序

### 体验

![gh_b0baa6d324a3_258](static/images/README/gh_b0baa6d324a3_258.jpg)

### 效果预览



![img](static/images/README/AA98670B08957CE2199109E24FCF6DBD-16886445334391.jpg)
![img](static/images/README/4C0A85A2458D2A30E386FEFD4923ED44.jpg)
![img](static/images/README/0D815D6E7206D00BB8BD6C02303FA24B.jpg)
