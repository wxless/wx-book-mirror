// pages/detail/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    book: null
  },
  // 赋值
  copy1() {
    let CP1 = 'https://static2.file123.info' + this.data.book.downloadUrl
    wx.setClipboardData({
      data: CP1,
      success: res => {
        wx.showToast({
          title: '下载地址已复制',
          icon: 'success'
        })
        // 在页面中定义插屏广告
        let interstitialAd = null

        // 在页面onLoad回调事件中创建插屏广告实例
        if (wx.createInterstitialAd) {
          interstitialAd = wx.createInterstitialAd({
            adUnitId: 'adunit-5768661dc9dd1127'
          })
          interstitialAd.onLoad(() => { })
          interstitialAd.onError((err) => { })
          interstitialAd.onClose(() => { })
        }

        // 在适合的场景显示插屏广告
        if (interstitialAd) {
          interstitialAd.show().catch((err) => {
            console.error(err)
          })
        }
      }
    })
  },
  copy2() {
    let CP2 = 'https://static.file123.info:8443' + this.data.book.downloadUrl
    wx.setClipboardData({
      data: CP2,
      success: res => {
        wx.showToast({
          title: '下载地址已复制',
          icon: 'success'
        })
        // 在页面中定义插屏广告
let interstitialAd = null

// 在页面onLoad回调事件中创建插屏广告实例
if (wx.createInterstitialAd) {
  interstitialAd = wx.createInterstitialAd({
    adUnitId: 'adunit-5768661dc9dd1127'
  })
  interstitialAd.onLoad(() => {})
  interstitialAd.onError((err) => {})
  interstitialAd.onClose(() => {})
}

// 在适合的场景显示插屏广告
if (interstitialAd) {
  interstitialAd.show().catch((err) => {
    console.error(err)
  })
}
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let book = wx.getStorageSync('book')
    console.log(book)
    this.setData({
      book
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})