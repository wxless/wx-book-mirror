Page({
  data: {
    activeName: null,
  },
  onChange(event) {
    this.setData({
      activeName: event.detail,
    });
  },
});
