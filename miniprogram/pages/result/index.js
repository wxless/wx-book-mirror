// pages/result/index.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    books:[]
  },
  // 选中图书
  tapBook(e){
    console.log(e)
    let tapBook = e.currentTarget.dataset.item;
    wx.setStorageSync('book', tapBook)
    wx.navigateTo({
      url: '../detail/index',
    })
  },
  // 请求图书列表
  reqBooks(key){
    wx.showLoading({
      title: '加载中',
    })
    wx.cloud.callFunction({
      name:'bookSearch',
      data:{
        key
      },
      success:res=>{
        console.log(res)
        this.setData({
          books:res.result
        })
      },
      complete:res=>{
        console.log(res)
        wx.hideLoading();
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let key = options.key;
    console.log(key)
    this.reqBooks(key)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})