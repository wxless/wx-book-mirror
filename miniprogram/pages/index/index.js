Page({

  /**
   * 页面的初始数据
   */
  data: {
    agree: false,
    // 是否展示协议
    show: false
  },
  showCode(){
    wx.previewImage({
      urls: ['cloud://book2-7gcm95g0c64067b2.626f-book2-7gcm95g0c64067b2-1319193207/PIC/code.jpg'],
    })
  },
  // 跳转到帮助页面
  toHelp() {
    wx.navigateTo({
      url: '../help/index',
    })
  },
  onSearch(e) {
    //console.log(e)
    let key = e.detail;
    wx.navigateTo({
      url: '../result/index?key=' + key,
    })
  },
  // 监听聚焦事件
  onFocus() {
    if (!this.data.agree) {
      wx.showModal({
        title: '使用协议',
        content: '版权声明\r\n本站数据均收集自互联网公开内容，版权归原作者所有，仅供学习参考使用、禁止用于商业用途，如无意中侵犯了任何媒体、公司、企业或个人等的知识产权，请联系删除(SDSH44)，本平台将不承担任何责任。 \r\n免责声明\r\n本平台对转载、分享的内容、陈述、观点判断保持中立，不对所包含内容的准确性、可靠性或完善性提供任何明示或暗示的保证，仅供参考，本平台将不承担任何责任。以上内容的最终解释权归本平台所有。',
        confirmText: '同意',
        showCancel:false,
        complete: (res) => {
          if (res.cancel) {

          }

          if (res.confirm) {
            wx.setStorageSync('agree', true)
            this.setData({
              agree: true
            })
          }
        }
      })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 查询
    let agree = wx.getStorageSync('agree') || false
    this.setData({
      agree
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})